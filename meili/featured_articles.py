# featured_articles.py

from bs4 import BeautifulSoup
import pandas as pd
import requests
from tqdm import tqdm

url = 'https://en.wikipedia.org/wiki/Wikipedia:Featured_articles'
urls = [url]

all_titles = []


def get_featured_titles(url):
    """ get titles from https://en.wikipedia.org/wiki/Wikipedia:Featured_articles """

    response = requests.get(url)
    content = response.content
    soup = BeautifulSoup(content, 'html.parser')

    for link in soup.find_all('a'):
        if link.get('href') and link.get('href').startswith('/wiki/'):
            title = link.text.strip()
            if title and title not in all_titles:
                all_titles.append(title)
    return all_titles


for url in tqdm(urls):
    get_featured_titles(url)


data = pd.DataFrame({'page_title': all_titles})
data.to_csv('data/wiki_featured_titles.csv', index=False)
