# utils.py

import uuid


def create_uuid(from_string):
    my_uuid = uuid.uuid5(uuid.NAMESPACE_DNS, from_string)
    return str(my_uuid)
