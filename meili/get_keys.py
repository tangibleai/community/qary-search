import json
import meilisearch
import os

MEILI_MASTER_KEY = os.environ['MEILI_MASTER_KEY']

master_client = meilisearch.Client('https://docker-meilisearch.onrender.com', MEILI_MASTER_KEY)
response = master_client.get_keys()
print(response)

api_key = response['results'][1].get('key')
print(api_key)


client = meilisearch.Client('https://docker-meilisearch.onrender.com', api_key)


# example search
star_wars = client.index('file').search('StarWars')
print(star_wars)

# example upload
json_file = open('file.json')
movies = json.load(json_file)
client.index('movies').add_documents(movies)


# create key
# https://docs.meilisearch.com/reference/api/keys.html#create-a-key

# client.create_key(options={
#   'description': 'Add documents: Products API key',
#   'actions': ['documents.add'],
#   'indexes': ['products'],
#   'expiresAt': '2042-04-02T00:42:42Z'
# })

# delete index
master_key = os.environ['MEILI_MASTER_KEY']
client = meilisearch.Client('https://docker-meilisearch.onrender.com', master_key)
client.index('wiki').delete()
