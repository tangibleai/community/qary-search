# data_dump.py

import os
import paramiko
import requests

# create data dump

url = 'https://docker-meilisearch.onrender.com/dumps'
headers = {
    'Content-Type': 'application/json',
    'Authorization': os.environ['MEILI_API_KEY']
}
response = requests.post(url, headers=headers)

print('backup data dump created!')
print(response.text)

# run commands over ssh

hostname = 'ssh.oregon.render.com'
username = 'srv-cfensb1mbjsqnjgcf9l0'
key_filename = '/home/john/.ssh/render/id_ed25519'
commands = ['pwd', 'ls -l', 'df -h', 'uptime']

# create SSH client and load private key
ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
private_key = paramiko.Ed25519Key.from_private_key_file(key_filename)

# connect to remote server with public key authentication
ssh.connect(hostname=hostname, username=username, pkey=private_key)

print("we're in!")
# execute command to list directories
stdin, stdout, stderr = ssh.exec_command('pwd')

# execute commands and print output
for cmd in commands:
    print(f'Executing command: {cmd}')
    stdin, stdout, stderr = ssh.exec_command(cmd)
    for line in stdout.readlines():
        print(line.strip())

# close SSH connection
ssh.close()

# TODO copy data dump to cloud storage
# # https://render.com/docs/disks

# # copying a file from your service to your local machine
# scp -s YOUR_SERVICE@ssh.YOUR_REGION.render.com:/path/to/remote/file /destination/path/for/local/file
# # file        100% 5930KB 999.9KB/s   00:05

# # copying a file from your local machine to your service
# scp -s /path/to/local/file YOUR_SERVICE@ssh.YOUR_REGION.render.com:/destination/path/for/remote/file
# # file        100% 5930KB 999.9KB/s   00:05
