# good_articles.py

from bs4 import BeautifulSoup
import pandas as pd
import requests
from tqdm import tqdm

url = 'https://en.wikipedia.org/wiki/Wikipedia:Good_articles/all'
url2 = 'https://en.wikipedia.org/wiki/Wikipedia:Good_articles/Social_sciences_and_society'
url3 = 'https://en.wikipedia.org/wiki/Wikipedia:Good_articles/Sports_and_recreation'
url4 = 'https://en.wikipedia.org/wiki/Wikipedia:Good_articles/Video_games'
url5 = 'https://en.wikipedia.org/wiki/Wikipedia:Good_articles/Warfare'
urls = [url, url2, url3, url4, url5]

all_titles = []


def get_good_titles(url):
    """ get titles from https://en.wikipedia.org/wiki/Wikipedia:Good_articles/ """

    response = requests.get(url)
    content = response.content
    soup = BeautifulSoup(content, 'html.parser')

    for link in soup.find_all('a'):
        if link.get('href') and link.get('href').startswith('/wiki/'):
            title = link.text.strip()
            if title and title not in all_titles:
                all_titles.append(title)
    return all_titles


for url in tqdm(urls):
    get_good_titles(url)


data = pd.DataFrame({'page_title': all_titles})
data.to_csv('data/wiki_good_titles.csv', index=False)
