from datetime import datetime
from django.utils import timezone

from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.hashers import check_password

from PIL import Image


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    index_name = models.CharField(max_length=120, blank=True, null=True, default=None)
    image = models.ImageField(default='default.jpg', upload_to='profile_pics')
    api_key = models.CharField(max_length=120, blank=True, null=True, default=None)
    api_secret = models.CharField(max_length=140, blank=True, null=True, default=None)

    # max_length set to 10485760 because it is postgresql max string length #
    synonyms = models.CharField(max_length=10485760, blank=True, null=True, default=None)
    stopWords = models.CharField(max_length=10485760, blank=True, null=True, default=None)
    rankingRules = models.CharField(max_length=10485760, blank=True, null=True, default=None)
    searchableAttributes = models.CharField(max_length=10485760, blank=True, null=True, default=None)
    pagination = models.CharField(max_length=10485760, blank=True, null=True, default=None)
    typoTolerance = models.CharField(max_length=10485760, blank=True, null=True, default=None)
    filterableAttributes = models.CharField(max_length=10485760, blank=True, null=True, default=None)

    def __str__(self):
        return f'Profile({self.user.username}, {self.user.email})'

    def has_valid_api_secret(self, secret_key: str) -> bool:
        return check_password(secret_key, self.api_secret)

    def save(self, *args, **kwargs):
        super(Profile, self).save(*args, **kwargs)

        img = Image.open(self.image.path)

        if img.height > 300 or img.width > 300:
            outout_size = (300, 300)
            img.thumbnail(outout_size)
            img.save(self.image.path)

    class Meta:
        managed = True


class Tag(models.Model):
    created = models.DateTimeField(default=timezone.now, help_text="Domain identifier")
    modified = models.DateTimeField(default=timezone.now, help_text="Domain identifier")
    name = models.CharField(max_length=100, blank=True, null=True, default=None)


class Synonym(models.Model):
    text = models.CharField(max_length=100)
    synonym = models.ManyToManyField('self')
    tag = models.ManyToManyField(Tag, through='SynonymTag')


class SynonymTag(models.Model):
    tag = models.ForeignKey(Tag, on_delete=models.CASCADE)
    synonym = models.ForeignKey(Synonym, on_delete=models.CASCADE)