# django-api-keys/users/urls.py

from dj_rest_auth.views import LoginView, LogoutView
from django.urls import path

from .views import APIKeysView, ResetAPIKeysView

urlpatterns = [
    path('api-keys/', APIKeysView.as_view(), name='api-keys'),
    path('reset-api-keys/', ResetAPIKeysView.as_view(), name='reset-api-keys'),
]