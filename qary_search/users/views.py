import json

from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.utils.crypto import get_random_string
from django.conf import settings as conf_settings
from meilisearch import Client
from pyasn1.compat.octets import null
from rest_framework import status
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import datetime, timezone, timedelta

from users.forms import UserRegisterForm, UserUpdateForm, ProfileUpdateForm
from .models import Profile

import random
import string
import requests


def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()

            username = form.cleaned_data.get('username')
            messages.success(request, f'Account Created for {username}! You can now log in.')
            return redirect('login')
    else:
        form = UserRegisterForm()
    return render(request, 'users/register.html', {'form': form})


@login_required
def profile(request):
    if request.method == 'POST':
        if 'update' in request.POST:
            u_form = UserUpdateForm(request.POST, instance=request.user)
            p_form = ProfileUpdateForm(request.POST,
                                       request.FILES,
                                       instance=request.user.profile)
            if u_form.is_valid() and p_form.is_valid():
                u_form.save()
                p_form.save()

                username = u_form.cleaned_data.get('username')
                messages.success(request, f'Account Updated for {username}!')
                return redirect('profile')
    u_form = UserUpdateForm(instance=request.user)
    p_form = ProfileUpdateForm(instance=request.user.profile)

    context = {
        'u_form': u_form,
        'p_form': p_form
    }


    user = Profile.objects.get(id=request.user.id)
    key = get_api_keys(user)

    if key:
        context['api_key'] = key

    if 'reset' in request.POST:
        reset_key = reset_api_keys(user)
        context['api_key'] = reset_key
        return render(request, 'users/profile.html', context)

    return render(request, 'users/profile.html', context)


def generate_random_string(length: int) -> str:
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=length))


def get_current_date():
    now = datetime.now(timezone.utc)
    one_year_later = now + timedelta(days=365)
    return one_year_later.strftime("%Y-%m-%dT%H:%M:%SZ")


def generate_api_key(username):
    meili_api_key = conf_settings.SECRET_KEY
    meili_url = conf_settings.MEILI_INSTANCE_URL
    current_date = get_current_date()

    url = f'{meili_url}'

    client = Client(url, meili_api_key)
    response = client.create_key(options={
        'description': 'Search patient records key',
        'actions': ['search'
                    # 'documents.add',
                    # 'documents.delete',
                    # 'documents.get',
                    # 'documents.delete',
                    # 'indexes.create',
                    # 'indexes.get',
                    # 'indexes.update',
                    # 'indexes.delete',
                    # 'indexes.swap',
                    # 'keys.get',
                    # 'keys.create',
                    # 'keys.update',
                    # 'keys.delete'
        ],
        'indexes': ['username'],
        'expiresAt': current_date
    })
    # probably should be changed to -> "expiresAt": null OR -> "expiresAt": "2024-01-01T00:00:00Z"
    data_decoded = response.json()
    data_json = json.loads(data_decoded)
    api_key = data_json['key']
    index_name = data_json['uid']
    return api_key, index_name


class APIKeysView(APIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication, TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request):
        print(request.user)
        user = Profile.objects.get(user=request.user)
        if user:
            if user.api_key:
                return Response(data={"api_key": user.api_key}, status=status.HTTP_200_OK)
            else:
                capsule = generate_api_key(user.user)
                if not capsule:
                    print("Error: We do not get any API key")
                    return Response(status=status.HTTP_400_BAD_REQUEST)
                api_key = capsule[0]
                index_name = capsule[1]
                api_secret = generate_random_string(64)
                user.api_key = api_key
                user.api_secret = api_secret
                user.index_name = index_name
                user.save()
                return Response(data={"api_key: ": api_key, "api_secret": api_secret}, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)

    def post(self, request):
        user = Profile.objects.get(id=request.user.id)
        if user:
            api_key = generate_api_key(user.user)
            api_secret = generate_random_string(64)
            user.api_key = api_key
            user.api_secret = api_secret
            user.save()
            return Response(data={"api_key": api_key}, status=status.HTTP_200_OK)


# New
class ResetAPIKeysView(APIView):
    permission_classes = [AllowAny]

    def get(self, request):
        api_s = request.GET.get('api_s', '')
        user = Profile.objects.get(api_secret=api_s)
        if not user:
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        print(user.user)
        api_key, index_name = generate_api_key(user.user)
        api_secret = generate_random_string(64)
        user.api_key = api_key
        user.api_secret = api_secret
        user.save()
        return Response(data={"api_key": api_key, "api_secret": api_secret}, status=status.HTTP_200_OK)


def get_api_keys(user):
    if user:
        if user.api_key:
            context = user.api_key
        else:
            capsule = generate_api_key()
            if not capsule:
                context = 'We could not generate a key at this time.'
            api_key = capsule[0]
            index_name = capsule[1]
            api_secret = generate_random_string(64)
            user.api_key = api_key
            user.api_secret = api_secret
            user.index_name = index_name
            user.save()
            context = api_key
    if context:
        return context
    return None


def reset_api_keys(user):
    if user:
        capsule = generate_api_key()
        if not capsule:
            context = 'We could not reset your key at this time.'
        api_key = capsule[0]
        index_name = capsule[1]
        api_secret = generate_random_string(64)
        user.api_key = api_key
        user.api_secret = api_secret
        user.index_name = index_name
        user.save()
        context = api_key
    if context:
        return context
    return None