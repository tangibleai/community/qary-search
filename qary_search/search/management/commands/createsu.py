# search/management/commands/createsu.py
import os

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Creates a superuser.'

    def handle(self, *args, **options):
        admin_password = os.getenv('admin_password')
        if not User.objects.filter(username='admin').exists():
            User.objects.create_superuser(
                username='admin',
                password=admin_password
            )
        print('Superuser has been created.')
