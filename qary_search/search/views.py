from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, FileResponse
from django.shortcuts import render
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from search.forms import SearchForm, UploadFileForm, UploadWikiTitleForm
from search.haystack import extract_qa
from search.meili.upload_data import upload_json
from search.meili.utils import BytesJSONEncoder
from search.meili.file_data import dict_constructor, dict_constructor_text, dict_constructor_csv
from search.meili.wiki_data import get_data
from search.meili_search import search_meili
from search.serializers import QuerySerializer
from search.tasks import long_running_task

from users.models import Profile
import pydoxtools as pdx
import requests
import csv
import io
import json


def get_documents(request, scope):
    index_name= None
    if scope == "private":
        user = Profile.objects.get(id=request.user.id)
        index_name = user.index_name
    elif scope == "community":
        index_name = scope
    elif scope == "public":
        index_name = "wiki"
    else:
        index_name = "wiki"
    results = None
    response = search_meili(index_name=index_name, limit=10)
    if response:
        json_response = json.loads(response.text)
        hits = json_response.get('hits')
        counter_id = 0
        data = []
        for hit in hits:
            if hit.get('title') and hit.get('url') and hit.get('summary'):
                data_dict = {
                    'id': counter_id,  # Assuming there's an 'id' field for each result
                    'title': hit.get('title'),
                    'url': hit.get('url'),
                    'summary': f"{hit.get('summary')[:255].strip()}..."
                }
                data.append(data_dict)
                counter_id += 1

        if data:
            results = data
            request.session['search_results'] = results
        else:
            results = ['No results found']
            render(request, 'search/empty_results.html')
    else:
        results = ['Could not reach Meili']
        render(request, 'search/empty_results.html')

    context = {
        'results': results
    }
    return render(request, 'search/results.html', context)


def download(request, result_id):
    search_results = request.session.get('search_results')
    for el in search_results:
        if el['id'] == result_id:
            title = el['title']
            data = get_data(title)
            json_str = json.dumps(data)
            response = HttpResponse(json_str, content_type='application/json')
            response['Content-Disposition'] = f'attachment; filename="{title}.json"'
            return response
    messages.warning(request, 'We did not find a Wiki article with that title.')
    return render(request, 'search/search.html', search_results)


def download_all(request, input_string):
    data = {}
    search_results = request.session.get('search_results')
    for el in search_results:
        title = el['title']
        wiki_data = get_data(title)
        data[title] = wiki_data
    json_str = json.dumps(data)
    response = HttpResponse(json_str, content_type='application/json')
    response['Content-Disposition'] = f'attachment; filename="{input_string}_all.json"'
    if json_str:
        return response
    else:
        messages.warning(request, 'We did not find a Wiki article with that title.')
        return render(request, 'search/search.html')


def search(request):
    input_string = None
    results = None
    answer = None
    if request.method == 'POST':
        form = SearchForm(request.POST)
        if form.is_valid():
            input_string = form.cleaned_data['input_string']

            if input_string.endswith("?"):
                extracted = extract_qa(input_string[:-1])
                if extracted:
                    answer = extracted
                else:
                    answer = 'could not reach qary-search-haystack'

            else:

                response = search_meili(input_string)

                if response:
                    json_response = json.loads(response.text)
                    hits = json_response.get('hits')
                    counter_id = 0
                    data = []
                    for hit in hits:
                        if hit.get('title') and hit.get('url') and hit.get('summary'):
                            data_dict = {
                                'id': counter_id,  # Assuming there's an 'id' field for each result
                                'title': hit.get('title'),
                                'url': hit.get('url'),
                                'summary': f"{hit.get('summary')[:255].strip()}..."
                            }
                            data.append(data_dict)
                            counter_id += 1

                    if data:
                        results = data
                        # if request.session['search_results']:
                        #     del request.session['search_results']
                        request.session['search_results'] = results
                    else:
                        results = ['No results found']
                else:
                    results = ['Could not reach Meili']

    form = SearchForm()
    context = {
        'form': form,
        'results': results,
        'answer': answer,
        'input_string': input_string,
    }

    return render(request, 'search/search.html', context)


@login_required
def upload_data(request):
    if request.method == 'POST':
        context = {}
        sucess_message = 'Data uploaded!'
        file_form = UploadFileForm(request.POST, request.FILES)
        if file_form.is_valid() and request.FILES.get('file'):
            user = Profile.objects.get(id=request.user.id)
            file = request.FILES['file']
            if ".json" in file._name:
                upload_data = file.read()
                dict_upload_data = json.loads(upload_data)
                # long_running_task.delay(dict_upload_data)
                json_data = dict_constructor(input_dict=dict_upload_data, file_name=file._name)
                messages.success(request, 'Task started')
                context['file_form'] = file_form
                wiki_title_form = UploadWikiTitleForm()
                context['wiki_title_form'] = wiki_title_form
                if json_data:
                    upload_json(json_data, index_name=user.index_name, primary_key='uuid')
                    messages.success(request, sucess_message)
                    messages.warning(request,
                                     'The "status" is "enqueued", this may take a bit to process on the Meili server.')
                if not json_data:
                    messages.warning(request, 'We did not find a Wiki article with that title.')
                return render(request, 'search/upload_data.html', context)

            if ".csv" in file._name:
                data = io.TextIOWrapper(file, encoding='utf-8')
                dict_data = csv.DictReader(data)
                # long_running_task.delay(dict_data)
                csv_data = dict_constructor_csv(input_dict=list(dict_data), file_name=file._name)
                messages.success(request, 'Task started')
                context['file_form'] = file_form
                wiki_title_form = UploadWikiTitleForm()
                context['wiki_title_form'] = wiki_title_form
                if csv_data:
                    upload_json(csv_data, index_name=user.index_name, primary_key='uuid')
                    messages.success(request, sucess_message)
                    messages.warning(request,
                                     'The "status" is "enqueued", this may take a bit to process on the Meili server.')
                if not csv_data:
                    messages.warning(request, 'We did not find a Wiki article with that title.')
                return render(request, 'search/upload_data.html', context)

            if ".pdf" or ".py" or ".html" or ".md" in file._name:
                data = file.read()
                doc = pdx.Document(fobj=data)
                # long_running_task.delay(doc)
                dict = dict_constructor_text(summary=doc.x('clean_text'), file_name=file._name)
                if dict:
                    upload_json(dict, index_name=user.index_name, primary_key='uuid')
                    messages.success(request, sucess_message)
                    messages.warning(request,
                                     'The "status" is "enqueued", this may take a bit to process on the Meili server.')
                if not dict:
                    messages.warning(request, 'We did not find a Wiki article with that title.')
                return render(request, 'search/upload_data.html', context)

            file_form = UploadFileForm()
            context['file_form'] = file_form
            render(request, 'search/upload_data.html', context)
        else:
            pass

        wiki_title_form = UploadWikiTitleForm(request.POST)
        if wiki_title_form.is_valid() and wiki_title_form.cleaned_data['wiki_title']:

            wiki_title = wiki_title_form.cleaned_data['wiki_title']
            data = get_data(wiki_title)
            user = Profile.objects.get(id=request.user.id)
            print(user.index_name)
            if data:
                upload_json(data, index_name=user.index_name, primary_key='uuid')
                messages.success(request, sucess_message)
                messages.warning(request, 'The "status" is "enqueued", this may take a bit to process on the Meili server.')
            if not data:
                messages.warning(request, 'We did not find a Wiki article with that title.')
            wiki_title_form = UploadWikiTitleForm()
            context['wiki_title_form'] = wiki_title_form
            render(request, 'search/upload_data.html', context)
        else:
            pass

    else:
        file_form = UploadFileForm()
        wiki_title_form = UploadWikiTitleForm()
        wiki_title_form

    context = {'file_form': file_form,
               'wiki_title_form': wiki_title_form
               }
    return render(request, 'search/upload_data.html', context)


class ApiView(APIView):
    permission_classes = [AllowAny]

    def get(self, request, format=None):
        param = request.GET.get('q', '')
        api = request.GET.get('api', '')
        user = Profile.objects.get(api_key=api)
        if not user:
            print("no api key in db")
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        response = search_meili(param)

        if response:
            json_response = json.loads(response.text)
            hits = json_response.get('hits')

            data = []
            for hit in hits:
                if hit.get('title') and hit.get('url') and hit.get('summary'):
                    data_dict = {'title': hit.get('title'),
                                 'url': hit.get('url'),
                                 'summary': f"{hit.get('summary')[:255].strip()}..."
                                 }

                    data.append(data_dict)

            if data:
                results = data
            else:
                results = ['No results found']
        else:
            results = ['could not reach mieli']

        context = {
            'query': param,
            'results': results,
        }

        data = {'context': context}
        serializer = QuerySerializer(data)
        return Response(serializer.data)
