
from celery import shared_task

from search.meili.utils import handle_uploaded_file
from search.meili.process_chunks import process_chunks


@shared_task
def long_running_task(file):
    handle_uploaded_file(file)
    process_chunks(filename='upload.csv')
