# process_chunks.py

import logging

import pandas as pd
from pathlib import Path
from tqdm import tqdm

from django.conf import settings

from search.meili.upload_data import upload_json
from search.meili.wiki_data import get_data

logger = logging.getLogger('process_chunks')


def process_chunks(filename, chunksize=100, start_index=0):
    ''' get titles in batches and get their wiki data and upload to meilisearch '''
    base_dir = settings.BASE_DIR
    file_path = Path(base_dir, 'search/meili/data/upload.csv')

    with open(file_path) as f:
        num_rows = sum(1 for line in f) - start_index
        logger.info(f"number of rows = {num_rows}")

    pbar = tqdm(total=num_rows)
    df = pd.read_csv(file_path)

    num_chunks_to_skip = start_index // chunksize
    rows_to_skip = start_index % chunksize

    for i, chunk in enumerate(pd.read_csv(file_path,
                                          chunksize=chunksize,
                                          header=None,
                                          skiprows=range(1, rows_to_skip + 1))):

        if i < num_chunks_to_skip:
            continue

        list_of_dicts = []
        column_values = chunk.iloc[:, 0].values

        for value in column_values:
            data = get_data(value)
            if data:
                list_of_dicts.append(data)

        if list_of_dicts:
            upload_json(list_of_dicts, index_name='wiki', primary_key='uuid')
            logger.info(f"last title processed was {value}")

            last_uploaded = list_of_dicts[-1].get('title')
            last_index = df.index[df.iloc[:, 0] == last_uploaded][0]
            logger.info(f"last index processed was {last_index}")

        pbar.update(len(chunk))


if __name__ == "__main__":
    process_chunks(filename='data/wiki_featured_titles.csv')
