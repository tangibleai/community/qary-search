from search.meili.utils import create_uuid
from datetime import datetime, timezone, timedelta


def dict_constructor(input_dict, file_name, tags=""):
    """ add data for key-value format files and return data """

    now = datetime.now(timezone.utc)

    input_dict.update(
        dict(
            uuid=create_uuid(now.isoformat()),
            tags=tags,
            title=file_name
        ))
    return input_dict


def dict_constructor_csv(input_dict, file_name, tags=""):
    """ add data for key-value format files and return data """

    data = {}
    now = datetime.now(timezone.utc)

    data.update(
        dict(
            uuid=create_uuid(now.isoformat()),
            tags=tags,
            title=file_name,
            url="",
            summary=input_dict
        ))
    return data


def dict_constructor_text(summary, file_name, tags=""):
    """ add data for key-value format files and return data """

    data = {}
    now = datetime.now(timezone.utc)

    data.update(
        dict(
            uuid=create_uuid(now.isoformat()),
            tags=tags,
            title=file_name,
            url="",
            summary=summary
        ))
    return data
