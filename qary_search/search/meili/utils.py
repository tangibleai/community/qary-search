# utils.py
import json
import uuid
import pandas as pd
from pathlib import Path

from django.conf import settings
from django.core.serializers.json import DjangoJSONEncoder


def create_uuid(from_string):
    my_uuid = uuid.uuid5(uuid.NAMESPACE_DNS, from_string)
    return str(my_uuid)


def handle_uploaded_file(f):
    base_dir = settings.BASE_DIR
    filename = 'search/meili/data/upload.csv'
    my_file = Path(base_dir, filename)

    json_object = json.loads(f)
    titles = json_object.get('file_contents')
    data = [title.strip() for title in titles.split('\n') if title.strip()]

    df = pd.DataFrame({'page_title': data})
    df.to_csv(my_file, index=False)


class BytesJSONEncoder(DjangoJSONEncoder):
    def default(self, obj):
        if isinstance(obj, bytes):
            return obj.decode('utf-8')
        return super().default(obj)
