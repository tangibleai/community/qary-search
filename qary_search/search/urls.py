from django.urls import path
from search import views

urlpatterns = [
    path('', views.search, name='search'),
    path('get_documents/<scope>', views.get_documents, name='get_documents'),
    path('download/<int:result_id>/', views.download, name='download'),
    path('download-all/<input_string>/', views.download_all, name='download_all'),
    path('upload-data/', views.upload_data, name='upload_data'),
    path('api/', views.ApiView.as_view()),
]
