from django import forms


class SearchForm(forms.Form):
    input_string = forms.CharField(label='',
                                   widget=forms.TextInput(attrs={'autofocus': True,
                                                                 'style': 'width: 50vw; max-width: 100%; overflow: auto; white-space: nowrap;'
                                                                 }))


class UploadFileForm(forms.Form):
    file = forms.FileField(label='Upload File', required=False)


class UploadWikiTitleForm(forms.Form):
    wiki_title = forms.CharField(label='Wiki Title', required=False)
