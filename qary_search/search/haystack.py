import requests


def extract_qa(query):

	url = f"https://qary-search-haystack.onrender.com/extract_qa?question={query}"

	payload = {}
	headers = {}

	response = requests.request("GET", url, headers=headers, data=payload)
	
	if response:
		return(response.json())
	return None