# qary-search miniprojects

### Ingest any document
**Budget**: $100 

Goal: integrate Thomas's pydoxtools PyPi package into John's document ingestion/indexing and then test it on an account & index created for yourself that no others can see.
Story: User should be able to provide URLs to files rather than only using the `wikipedia` package to retrieve and parse wikipedia articles.

- Play with [pydoxtools](https://github.com/xyntopia/pydoxtools) 
 (Thomas's doxcavator.com biz from https://xyntopia.com/)
- Integrate the pydoxtool into qary-search
  - [X] add `pydoxtool` to the requrirements.txt for [qary-search](https://gitlab.com/tangibleai/community/qary-search/)
  - [X] move requirements from `requirements.txt` to `pyproject.toml` 
  - [X] get pyproject.toml working, and bump the minor version number to be able to install qary-search 
  - Add `pydoxtool` to the ['upload-data' `view.py`](https://search.qary.ai/upload-data/) in [`qary-search`](https://gitlab.com/tangibleai/community/qary-search/-/blob/main/qary_search/search/views.py#L63).
- [X] Add functions, that upload PDF, HTML, .py, .md, CSV 
- [X] Add ability to upload files and wiki titles by UUID (at the moment all the data is pushed in default "wiki" Index)

#### File format test cases
**Budget**: $100 

- [X] Test the upload-data view on these files to incrementally get it working on each one:

- PDF: [A Survey of Large Language Models](https://arxiv.org/pdf/2303.18223.pdf?fbclid=IwAR3GYBQ2P9Cww2HVM3oUbML9i5i3DMDBVv5_FvYWfEi-vdZqZoSM78jE2-s)
- PDF: [A Survey of Large Language Models](https://arxiv.org/pdf/2303.18223.pdf?fbclid=IwAR3GYBQ2P9Cww2HVM3oUbML9i5i3DMDBVv5_FvYWfEi-vdZqZoSM78jE2-s)
- PDF: [ChatGPT for Education](https://edarxiv.org/5er8f/download?format=pdf)
- HTML: [wikipedia.org/wiki/Large_language_model](https://en.wikipedia.org/wiki/Large_language_model)
- HTML: [Stanford HELM benchmarked models table](https://crfm.stanford.edu/helm/latest/?models=1)
- PDF: [Stanford HELM paper](https://arxiv.org/pdf/2211.09110.pdf)
- Python & Markdown: all py and markdown files in the `tangibleai/community/team` [GitLab repo](https://gitlab.com/tangibleai/community/team/-/blob/main/exercises/2-plan-your-prosocial-ai-project/prosocial-ai-project-ideas.yml)
- Make sure, that all the formats are supported. 

## Document information extraction 

### Private/public documents
**Budget**: $100 

  - [X] add feature to a user's profile to make their index and documents public, community, or private
    - Public - not logged in users
    - Private - personal files
    - Community - logged in users

### Data export
**Budget**: $100 

- [X] add download button to download ALL search results as json
  - add button 
  - add download function in views.py (Search)

- [X] add download/export button to download INDIVIDUALS documents as json
  - add button 
  - add download function in views.py (Search)

