# qary-search miniprojects

## Improve search results

- break wikipedia articles into sections and add sections to the index, with an id that includes the "parent" article document
- find a passage within the found documents that contains the search terms best
  - trigram tfidf vectorizer and rolling window over text

## Document information extraction 

### Data export
**Budget**: $100 

- add download button to download search results as json
- add download/export button to download individual documents as json

### Private/public documents
**Budget**: $100 

- add feature to a user's profile to make their index and documents public, community, or private
- upload some wikipedia pages about LLMs and NLP to your private index
- add some synonyms to make it easier to search
- check to make sure other users cannot see your documents
- make it community
- make sure other logged in users can access your documents (but not the general public
- make your documents public and test to make sure everyone can search the documents

### Ingest any document
**Budget**: $100 

Goal: integrate Thomas's pydoxtools PyPi package into John's document ingestion/indexing and then test it on an account & index created for yourself that no others can see.

- Play with [pydoxtools](https://github.com/xyntopia/pydoxtools) 
 (Thomas's doxcavator.com biz from https://xyntopia.com/)
- Integrate the pydoxtool into qary-search
  - add `pydoxtool` to the requrirements.txt for [qary-search](https://gitlab.com/tangibleai/community/qary-search/)
  - move requirements from `requirements.txt` to `pyproject.toml` 
  - get pyproject.toml working, and bump the minor version number to be able to install qary-search

Add `pydoxtool` to the ['upload-data' `view.py`](https://search.qary.ai/upload-data/) in [`qary-search`](https://gitlab.com/tangibleai/community/qary-search/-/blob/main/qary_search/search/views.py#L63).
User should be able to provide URLs to files rather than only using the `wikipedia` package to retrieve and parse wikipedia articles.

#### File format test cases
**Budget**: $100 

Test the upload-data view on these files to incrementally get it working on each one:

- PDF: [A Survey of Large Language Models](https://arxiv.org/pdf/2303.18223.pdf?fbclid=IwAR3GYBQ2P9Cww2HVM3oUbML9i5i3DMDBVv5_FvYWfEi-vdZqZoSM78jE2-s)
- PDF: [A Survey of Large Language Models](https://arxiv.org/pdf/2303.18223.pdf?fbclid=IwAR3GYBQ2P9Cww2HVM3oUbML9i5i3DMDBVv5_FvYWfEi-vdZqZoSM78jE2-s)
- PDF: [ChatGPT for Education](https://edarxiv.org/5er8f/download?format=pdf)
- HTML: [wikipedia.org/wiki/Large_language_model](https://en.wikipedia.org/wiki/Large_language_model)
- HTML: [Stanford HELM benchmarked models table](https://crfm.stanford.edu/helm/latest/?models=1)
- PDF: [Stanford HELM paper](https://arxiv.org/pdf/2211.09110.pdf)
- Python & Markdown: all py and markdown files in the `tangibleai/community/team` [GitLab repo](https://gitlab.com/tangibleai/community/team/-/blob/main/exercises/2-plan-your-prosocial-ai-project/prosocial-ai-project-ideas.yml)

