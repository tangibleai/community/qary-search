## DRF-auth 

**User story**: User would like to be able to create and reset their API keys that they can use to access the qary-search programmatically (for use within an external chatbot)  

#### Implementation suggestion: [django-auth keys stored in user Profile](https://12ft.io/proxy?&q=https%3A%2F%2Flevelup.gitconnected.com%2Fhow-to-create-user-api-keys-in-django-from-scratch-c1de15aec627)
- [x] create fields in user Profile model and view for managing API auth keys
- [x] protect the search API endpoint with user API keys (only valid keys should be able to search)

## How to test

### Create API keys
1. Register an account
2. Login in to account
3. Go to /users/api-keys/ to create api and secret keys </br>
(if you reload the page you will never be able to see the secret key again, except db)
4. Save your secret key to be able test next step
5. Check API keys in db
### Reset API keys
1. Get request to /users/reset-api-keys/?api_s=YOUR_SECRET_KEY </br>
(if you reload the page you will never be able to see the secret key again, except db)
2. Check API keys in db
### Search programmatically with API keys
1. Get request to /api/?q=YOUR+REQUEST&api=YOUR_API_KEY
2. Enjoy the result :)

### meilisearch keys

**User story**: User would like to be able to create and reset their meilisearch API keys index that they can use to programmatically access the meilisearch server.  

#### Implementation Suggestion:
- [X] use the meilisearch API and master key to generate individual user keys within the user Profile view in Django

## How to test
1. This task has modified task 1. To test it rerun all DRF-auth testing steps
2. Cooperation app with meilisearch API should be tested with the correct master key

### Personalized search indexes

**User story**: User would like to be able to have their own private index on meilisearch for uploading their documents  

- [X] add user `Profile.index_name` field to the user containing the user's private index name (can be read-only and can be the same as the username or user e-mail address)
- [X] add user `Profile` fields that allow the user to configure their Meilisearch index [meilisearch docs](https://www.meilisearch.com/docs/learn/configuration/settings):  
  * synonyms
  * stopWords
  * rankingRules
  * searchableAttributes
  * pagination
  * typoTolerance
  * faceting
  * filterableAttributes
#### Done

### Pay attention to .env.example and build.sh files changes

**User story**: User would like to multi select 3 custom synonym lists created by Tangible AI for at least 4 different "domains": general, software development, machine learning, natural language processing  

- [X] add `Tag` table to models.py: help_text or description in meta data: "Domain identifier"
  * created = DateTimeField
  * created = DateTimeField
- [X] add `Synonym` table to models.py
  * text = CharField(max_length=100)
  * synonym = ManyToMany(self)  # creates a DAG
  * tag = ManyToMany(Tag, through=SynonymTag)
- [X] automatically load fixture containing `Tag`s [general, software development, machine learning, natural language processing]
- [X] automatically load fixture containing at least 10 `Synonym`s in for each of those domains (tagged with the appropriate tags)

#### Run following commands to import tags and synonyms fixtures
python manage.py loaddata fixtures_tag.json </br>
python manage.py loaddata fixtures_synonym.json </br>