# qary-search

add summary here


### Developer install

First retrieve a copy of the source code:

```bash
git clone git@gitlab.com:tangibleai/community/qary-search.git
cd qary-search
```

create activate and install requirements into a virtual environment

```bash
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```
